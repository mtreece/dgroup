# dgroup - distinguished group
# Copyright (C) 2021  M. Tyler Reece
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

LDLIBS = $(shell pkg-config --libs-only-l glib-2.0)
LDFLAGS = $(shell pkg-config --libs-only-L glib-2.0)
CPPFLAGS = $(shell pkg-config --cflags-only-I glib-2.0)

all: dgroup_test
.PHONY: all
dgroup.o: dgroup.h
dgroup_test: dgroup.o
