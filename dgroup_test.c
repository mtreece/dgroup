/*  dgroup - distinguished group
 *  Copyright (C) 2021  M. Tyler Reece
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dgroup.h"

#define ERROR(fmt, ...) \
do { \
	fprintf(stderr, "%s:%u- (ERROR) " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__); \
	exit(1); \
} while (0)

static union {
	struct {
		int metainit;
		const char *fname;
		int d; /* distinguished */
	} init;
	struct {
		int reserved;
		struct col {
			int valid;
			const char *fname;
			const char *val;
		} **rows;
	} data;
} data[] = {
#define INIT(field, distinguished) \
	{.init = {.metainit = 1, .fname = field, .d = distinguished}}
	/********************************/
	INIT("field1", 1),
	INIT("field2", 1),
	INIT("field3", 1),
	INIT("field4", 0),
	INIT("field5", 0),
	/********************************/

#define BEGIN_ROW() { .data = { .rows = (struct col *[]){
#define PAIR(field, valn) &(struct col){ .valid = 1, .fname = field, .val = valn, }
#define END_ROW() &(struct col){.valid = 0} }},}

	BEGIN_ROW()
		PAIR("field1", "1000"),
		PAIR("field2", "2000"),
		PAIR("field3", "3000"),
		PAIR("field4", "4000"),
		PAIR("field5", "5000"),
	END_ROW(),

	BEGIN_ROW()
		PAIR("field1", "1000"),
		PAIR("field2", "2000"),
		PAIR("field3", "3000"),
		PAIR("field4", "4000"),
		PAIR("field5", "5000"),
	END_ROW(),

	BEGIN_ROW()
		PAIR("field1", "1000"),
		PAIR("field2", "2000"),
		PAIR("field3", "3000"),
		PAIR("field4", "4001"),
		PAIR("field5", "5001"),
	END_ROW(),

	/* next batch: alter field1 */

	BEGIN_ROW()
		PAIR("field1", "1001"),
		PAIR("field2", "2000"),
		PAIR("field3", "3000"),
		PAIR("field4", "4001"),
		PAIR("field5", "5001"),
	END_ROW(),

	BEGIN_ROW()
		PAIR("field1", "1001"),
		PAIR("field2", "2000"),
		PAIR("field3", "3000"),
		PAIR("field4", "4002"),
		PAIR("field5", "5002"),
	END_ROW(),

	BEGIN_ROW()
		PAIR("field1", "1001"),
		PAIR("field2", "2000"),
		PAIR("field3", "3000"),
		PAIR("field4", "4003"),
		PAIR("field5", "5003"),
	END_ROW(),

	/* next batch: alter field2 */

	BEGIN_ROW()
		PAIR("field1", "1001"),
		PAIR("field2", "2001"),
		PAIR("field3", "3000"),
		PAIR("field4", "4001"),
		PAIR("field5", "5001"),
	END_ROW(),

	/* next batch: alter field3 */

	BEGIN_ROW()
		PAIR("field1", "1001"),
		PAIR("field2", "2001"),
		PAIR("field3", "3001"),
		PAIR("field4", "4001"),
		PAIR("field5", "5001"),
	END_ROW(),

#undef END_ROW
#undef PAIR
#undef BEGIN_ROW
#undef INIT
};

static int my_cb(void *priv, const char **fname, const void **val, size_t nelts)
{
	size_t i;
	printf("batch:\n");
	for (i = 0; i < nelts; ++i) {
		printf("\t%s: %s\n", fname[i], ((char **)val)[i]);
	}
	return 0;
}

static int strcmp_data(const void *s1, const void *s2, void *priv)
{
	if (s1 == s2) {
		return 0;
	}
	if (!s1) {
		return -1;
	}
	if (!s2) {
		return 1;
	}
	return strcmp((const char *)s1, (const char *)s2);
}

int main(int argc, char *argv[])
{
	int res;
	size_t i;
	struct dgroup *d;
	struct dgroup_entry *de;

	if ((res = dgroup_open(&d, strcmp_data))) {
		ERROR("");
	}

	for (i = 0; i < sizeof(data)/sizeof(data[0]); ++i) {
		typeof(data[0].init) *p = &data[i].init;
		if (!p->metainit) {
			break;
		}
		if ((res = dgroup_register_field(d, p->fname, p->d))) {
			ERROR("");
		}
	}

	for (; i < sizeof(data)/sizeof(data[0]); ++i) {
		typeof(data[0].data) *p = &data[i].data;
		typeof(data[0].data.rows) row;
		if (p->reserved) {
			/* all inits should have happened before now */
			ERROR("");
		}
		if ((res = dgroup_entry_start(d, &de))) {
			ERROR("");
		}
		for (row = data[i].data.rows; (*row)->valid; ++row) {
			const char *fname = (*row)->fname;
			const char *val = (*row)->val;
			if ((res = dgroup_entry_addv(de, fname, val))) {
				ERROR("");
			}
		}
		if ((res = dgroup_entry_finish(d, &de))) {
			ERROR("");
		}
	}

	if ((res = dgroup_batch(d, my_cb, NULL))) {
		ERROR("");
	}

	if ((res = dgroup_close(&d))) {
		ERROR("");
	}

	return EXIT_SUCCESS;
}
