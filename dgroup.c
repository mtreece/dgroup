/*  dgroup - distinguished group
 *  Copyright (C) 2021  M. Tyler Reece
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
#include "dgroup.h"

#include <stdlib.h>
#include <errno.h>

#include <glib.h>

struct dgroup {
	int initialized; // TODO: check init everywhere
	dgroup_compar compar;

	GHashTable *fields;
	GList *fields_list; /* to preserve registered field order */

	GList *entries;

	/* XXX: refactor to avoid this */
	/* temporary pass-around vars */
	void *tmp;
};

struct dgroup_entry {
	int initialized; // TODO: check init everywhere
	struct dgroup *d;
	GHashTable *pairs;
};

int dgroup_open(struct dgroup **d, dgroup_compar compar)
{
	struct dgroup *p;
	if (!d) {
		return -EINVAL;
	}
	if (!(p = *d = malloc(sizeof(**d)))) {
		return -ENOMEM;
	}

	if (!(p->fields = g_hash_table_new(g_str_hash, g_str_equal))) {
		return -ENOMEM;
	}

	p->fields_list = NULL;
	p->compar = compar;
	p->entries = NULL;
	p->tmp = NULL;

	p->initialized = 1;
	return 0;
}

int dgroup_close(struct dgroup **d)
{
	if (!d) {
		return -EINVAL;
	}
	if (!*d) {
		return 0;
	}

	// TODO: clean up memory leaks

	g_hash_table_destroy((*d)->fields);
	g_list_free((*d)->fields_list);

	free(*d);
	return 0;
}

int dgroup_register_field(struct dgroup *d, const char *fname, int distinguished)
{
	/* XXX: best way to generically handle int val (vs size_t) */
	(void) g_hash_table_insert(d->fields,
	                           (gpointer) fname,
	                           (void*) (size_t) distinguished);
	d->fields_list = g_list_prepend(d->fields_list, (void *)fname);

	return 0;
}

int dgroup_entry_start(struct dgroup *d, struct dgroup_entry **e)
{
	struct dgroup_entry *p;

	if (!d || !e) {
		return -EINVAL;
	}

	if (!(p = *e = malloc(sizeof(**e)))) {
		return -ENOMEM;
	}

	p->d = d;
	if (!(p->pairs = g_hash_table_new(g_str_hash, g_str_equal))) {
		free(p);
		return -ENOMEM;
	}
	// TODO: cleanup all ->pairs in dgroup_close

	d->entries = g_list_prepend(d->entries, p);

	p->initialized = 1;
	return 0;
}

int dgroup_entry_addv(struct dgroup_entry *e, const char *fname, const void *val)
{
	struct dgroup *d;

	if (!e) {
		return -EINVAL;
	}

	d = e->d;

	/* only register new entries if the fname is already registered */
	if (!g_hash_table_lookup_extended(d->fields, fname, NULL, NULL)) {
		return -EINVAL;
	}

	(void) g_hash_table_insert(e->pairs, (gpointer) fname, (gpointer) val);

	return 0;
}

int dgroup_entry_finish(struct dgroup *d, struct dgroup_entry **e)
{
	if (!d || !e) {
		return -EINVAL;
	}
	*e = NULL;
	return 0;
}

static int distp(struct dgroup *d, const char *fieldn)
{
	return (int) (size_t) g_hash_table_lookup(d->fields,
	                                          (gconstpointer) fieldn);
}

/* Sort and Merge Distinguished Fields */
static GList *s_m_d_f(struct dgroup *d, const struct dgroup_entry *a,
                      const struct dgroup_entry *b)
{
	const GHashTable *ap = a->pairs;
	const GHashTable *bp = b->pairs;
	GHashTable *tmp;
	GList *cfields; /* combined fields */
	GHashTableIter iter;
	gpointer key, value;

	if (!(tmp = g_hash_table_new(g_str_hash, g_str_equal))) {
		return NULL;
	}

	g_hash_table_iter_init(&iter, (GHashTable *)ap);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		if (!distp(d, key)) { /* non-distinguished */
			continue;
		}
		(void) g_hash_table_add(tmp, key);
	}

	g_hash_table_iter_init(&iter, (GHashTable *)bp);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		if (!distp(d, key)) { /* non-distinguished */
			continue;
		}
		(void) g_hash_table_add(tmp, key);
	}

	if (!(cfields = g_hash_table_get_keys(tmp))) {
		goto cleanup;
	}
	cfields = g_list_sort(cfields, g_str_equal);

cleanup:
	g_hash_table_destroy(tmp);

	return cfields;
}

/* compare entries */
static gint c_entries(gconstpointer a, gconstpointer b, gpointer user_data)
{
	struct dgroup *d = (struct dgroup *)user_data;
	const struct dgroup_entry *ae = (const struct dgroup_entry *)a;
	const struct dgroup_entry *be = (const struct dgroup_entry *)b;
	dgroup_compar compar = d->compar;
	int cres;
	GList *fields, *field;

	/* we have two entries here and need to compare them; algo:
	 *   for each field in \
	 *       (sorted
	 *         (merge (distinguished? (fields ae))
	 *                (distinguished? (fields be))))
	 *     va = get_value(ae, field)
	 *     vb = get_value(be, field)
	 *     c = compar(va, vb, user_data)
	 *     if (!c)
	 *       return c
	 *     // was equal? ... keep looping
	 *   return c
	 */
	fields = s_m_d_f(d, ae, be);

	for (field = fields; field; field = g_list_next(field)) {
		gpointer va = g_hash_table_lookup(ae->pairs, field);
		gpointer vb = g_hash_table_lookup(ae->pairs, field);
		cres = compar(va, vb, d->tmp);
		if (!cres) {
			goto cleanup;
		}
	}

cleanup:
	g_list_free(fields);
	return cres;
}

/* distinguished tuple equal? */
static int dteql(struct dgroup *d, GList *a, GList *b)
{
	dgroup_compar compar;
	GList *dists, *dist;
	GHashTable *atable, *btable;

	/* trivial speedup */
	if (a == b) {
		return 1;
	}

	compar = d->compar;

	if (!(dists = dist = s_m_d_f(d,
	                             (struct dgroup_entry *)a->data,
	                             (struct dgroup_entry *)b->data))) {
		return 0;
	}

	atable = ((struct dgroup_entry *)a->data)->pairs;
	btable = ((struct dgroup_entry *)b->data)->pairs;

	for (; dist; dist = g_list_next(dist)) {
		void *aval;
		void *bval;
		aval = g_hash_table_lookup(atable, dist->data);
		bval = g_hash_table_lookup(btable, dist->data);
		if (0 != compar(aval, bval, d->tmp)) {
			return 0;
		}
	}

	g_list_free(dists);

	return 1;
}

static void push_data(struct dgroup *d, GList **fnames, GList **vals, GList *entry, int incl_dist)
{
	GList *l;
	struct dgroup_entry *e = (struct dgroup_entry *)entry->data;

	/* TODO: the _last has to traverse the list; optimize somehow (e.g. by
	 * holding a ptr to the last elt for speedup of much-larger lists)
	 */

	/* loop over all registered fields */
	for (l = g_list_last(d->fields_list); l; l = g_list_previous(l)) {
		const char *fname = l->data;
		gpointer val;

		/* lookup the fname, val combo */
		if (!g_hash_table_lookup_extended(e->pairs, fname, NULL,
		                                  &val)) {
			/* not in this entry, so skip it */
			continue;
		}

		if (!incl_dist && distp(d, fname)) {
			continue;
		}

		*fnames = g_list_prepend(*fnames, (void *)fname);
		*vals = g_list_prepend(*vals, val);
	}
}

static int flush_data(struct dgroup *d, dgroup_cb cb, void *priv, GList **out_fields, GList **out_vals)
{
	size_t i;
	size_t nelts;
	int res = 0;
	char **out_fn_ary;
	void **out_val_ary;
	GList *entry;

	*out_fields = g_list_reverse(*out_fields);
	*out_vals = g_list_reverse(*out_vals);

	nelts = (size_t) g_list_length(*out_fields);

	out_fn_ary = malloc(nelts * sizeof(char *));
	out_val_ary = malloc(nelts * sizeof(void *));
	if (!out_fn_ary || !out_val_ary) {
		goto cleanup;
	}

	for (i = 0, entry = *out_fields; entry; entry = g_list_next(entry)) {
		out_fn_ary[i++] = entry->data;
	}

	for (i = 0, entry = *out_vals; entry; entry = g_list_next(entry)) {
		out_val_ary[i++] = entry->data;
	}

	if ((res = cb(priv, (const char **)out_fn_ary,
	              (const void **)out_val_ary, nelts))) {
		goto cleanup;
	}

cleanup:
	g_list_free(*out_vals);
	g_list_free(*out_fields);
	*out_vals = NULL;
	*out_fields = NULL;
	free(out_val_ary);
	free(out_fn_ary);
	return res;
}

int dgroup_batch(struct dgroup *d, dgroup_cb cb, void *priv)
{
	GList *entry;
	GList *ldr; /* leader: first row with leading tuple */
	GList *out_fields = NULL, *out_vals = NULL;
	int res;

	if (!d || !cb || !d->initialized) {
		return -EINVAL;
	}

	res = 0;

	d->tmp = priv;

	/* entries are now sorted */
	d->entries = g_list_sort_with_data(d->entries, c_entries, d);

	/* init accumulation lists */
	out_fields = out_vals = NULL;

	/* make 1st one the leader; & definitely want to push all data */
	ldr = d->entries;
	push_data(d, &out_fields, &out_vals, ldr, 1);

	for (entry = g_list_next(d->entries); entry; entry = g_list_next(entry)) {
		int incl_dist = 0;
		if (!dteql(d, ldr, entry)) {
			if ((res = flush_data(d, cb, priv, &out_fields,
			                      &out_vals))) {
				goto cleanup;
			}

			/* no longer eql w/ leader? on to next one... */
			ldr = entry;
			incl_dist = 1;
		}
		push_data(d, &out_fields, &out_vals, entry, incl_dist);
	}

	if ((res = flush_data(d, cb, priv, &out_fields, &out_vals))) {
		goto cleanup;
	}

cleanup:
	d->tmp = NULL;
	return res;
}
