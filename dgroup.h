/*  dgroup - distinguished group
 *  Copyright (C) 2021  M. Tyler Reece
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
#ifndef _DGROUP_H
#define _DGROUP_H

#include <stdlib.h>

struct dgroup;
struct dgroup_entry;

typedef int (*dgroup_compar)(const void *a, const void *b, void *priv);
int dgroup_open(struct dgroup **d, dgroup_compar compar);
int dgroup_close(struct dgroup **d);
int dgroup_register_field(struct dgroup *d, const char *fname, int distinguished);

int dgroup_entry_start(struct dgroup *d, struct dgroup_entry **e);
int dgroup_entry_addv(struct dgroup_entry *e, const char *fname, const void *val);
int dgroup_entry_finish(struct dgroup *d, struct dgroup_entry **e);

typedef int (*dgroup_cb)(void *priv, const char **fname, const void **val, size_t nelts);
int dgroup_batch(struct dgroup *d, dgroup_cb cb, void *priv);

#endif /* _DGROUP_H */
